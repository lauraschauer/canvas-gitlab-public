{-# LANGUAGE OverloadedStrings #-}

module Main (main) where

import Canvas
import Common
import Control.Monad
import Control.Monad.IO.Class
import qualified Data.Text as T
import GitLab
import Network.HTTP.Client
import System.Environment

main :: IO ()
main = do
  let cCourseId = "22222" -- Your Canvas Course ID

  -- Canvas Configuration
  cToken <- T.pack <$> getEnv "CANVAS_TOKEN"
  let securityScheme = bearerAuthenticationSecurityScheme cToken

  let canvasConfig =
        defaultConfiguration
          { configBaseURL = "https://canvas.hw.ac.uk/api",
            configSecurityScheme = securityScheme
          }

  -- In Canvas context
  cUsers <- runWithConfiguration canvasConfig $ do
    -- Gets all users on a course
    let params =
          (mkListUsersInCourseUsersParameters cCourseId)
            { listUsersInCourseUsersParametersQueryInclude__ = Just ListUsersInCourseUsersParametersQueryInclude__'EnumSections -- requests the user's enrollments, in addition to the regular profile fields
            }
    userInfos <-
      do
        response <- listUsersInCourseUsers params
        case responseBody response of
          ListUsersInCourseUsersResponseError err -> do
            -- let the program crash, as there must be something wrong in the parameters
            error ("Couldn't get users for course: " <> show cCourseId <> ", reason: " <> err)
          ListUsersInCourseUsersResponse200 theProfiles -> return theProfiles

    return userInfos

  print (length cUsers)

  -- Obtain the username to match on GitLab
  let usernames = map (T.dropEnd 9 . safeNullNothing T.empty . userLoginId) cUsers

  -- GitLab Configuration
  gToken <- T.pack <$> getEnv "GITLAB_TOKEN"

  let gitlabConfig =
        defaultGitLabServer
          { url = "https://gitlab-student.macs.hw.ac.uk",
            token = gToken
          }

  -- In the context of GitLab
  void $
    runGitLab gitlabConfig $ do
      go usernames
  where
    go :: [T.Text] -> GitLab [GitLab.User]
    go [] = return []
    go (username : xs) = do
      result <- searchUser username
      case result of
        -- No user with that username on GitLab, print to console
        Nothing -> do
          liftIO (print (T.pack "No GitLab user with the username " <> username))
          go xs
        -- Good, found the user on GitLab
        Just gUser -> do
          -- check their organization field
          case user_organization gUser of
            -- if there's Nothing, go to next user
            Nothing -> do
              go xs
            Just usrOrg
              -- H-Number found, so get rid of it
              | "H" `T.isPrefixOf` usrOrg -> do
                liftIO $ print (T.pack "H-Number set for " <> username) -- sanity check
                let params =
                      defaultUserFilters
                        { userFilter_organization = Just T.empty
                        }

                -- modifies the user
                void $ do
                  response <- modifyUser (user_id gUser) params
                  case response of
                    Left _ -> do
                      liftIO (print (T.pack "Could not modify " <> username))
                      return Nothing
                    Right Nothing -> do
                      liftIO (print (T.pack "Modified, but no user returned for " <> username))
                      return Nothing
                    Right (Just theUser) -> return (Just theUser)

                -- modifies the rest of the list
                go xs

              -- no need to change if there's no H-Number in it
              | otherwise -> do
                liftIO $ print (T.pack "No H-Number was set for " <> username) -- sanity check
                go xs
