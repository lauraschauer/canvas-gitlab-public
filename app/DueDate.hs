{-# LANGUAGE OverloadedStrings #-}

module Main
  ( main,
  )
where

import Canvas as C
import Common
import Control.Monad
import Control.Monad.IO.Class
import Data.Maybe (catMaybes, fromJust)
import qualified Data.Text as T
import qualified Data.Time as T
import GitLab as G
import Network.HTTP.Client
import System.Environment

main :: IO ()
main = do
  let cAssignmentId = "111111" -- Your Canvas Assignment ID
  let cCourseId = "22222" -- Your Canvas Course ID
  let cEdiSectionId = 33333 -- Your Canvas Section ID

  -- Canvas Configuration
  cToken <- T.pack <$> getEnv "CANVAS_TOKEN"
  let securityScheme = bearerAuthenticationSecurityScheme cToken

  let canvasConfig =
        defaultConfiguration
          { configBaseURL = "https://canvas.hw.ac.uk/api", -- Your Canvas URL
            configSecurityScheme = securityScheme
          }

  -- GitLab Configuration
  gToken <- T.pack <$> getEnv "GITLAB_TOKEN"

  let gitlabConfig =
        defaultGitLabServer
          { url = "https://gitlab-student.macs.hw.ac.uk", -- Your GitLab URL
            token = gToken
          }

  -- In the context of Canvas
  (enrollments, dueDate) <- runWithConfiguration canvasConfig $ do
    -- Get all Edinburgh Students
    -- only obtain enrollments that are 'active' and a 'student'
    let enrollmentParams =
          (mkListEnrollmentsSectionsParameters cEdiSectionId)
            { listEnrollmentsSectionsParametersQueryState = Just ListEnrollmentsSectionsParametersQueryState'EnumActive,
              listEnrollmentsSectionsParametersQueryType = Just [T.pack "StudentEnrollment"]
            }
    ediStudents <- do
      response <- listEnrollmentsSections enrollmentParams
      case responseBody response of
        ListEnrollmentsSectionsResponseError err -> do
          -- let the program crash, as there must be something wrong in the parameters
          error ("Couldn't get enrollments for section: " <> (show cEdiSectionId) <> ", reason: " <> err)
        ListEnrollmentsSectionsResponse200 theEnrollments -> return theEnrollments

    -- Get the Assignment Due Date
    let assignmentParams = mkGetSingleAssignmentParameters cCourseId cAssignmentId

    maybeAssignment <- do
      response <- getSingleAssignment assignmentParams
      case responseBody response of
        GetSingleAssignmentResponseError _ -> return Nothing
        GetSingleAssignmentResponse200 theAssignment -> return (Just theAssignment)

    case maybeAssignment of
      Nothing -> do
        error (show $ "No assignment found for course " <> cCourseId <> " and assignment id " <> cAssignmentId)
      Just assignment -> do
        let string = safeNullNothing T.empty (assignmentDueAt assignment)
        case (T.parseTimeM True T.defaultTimeLocale "%Y-%m-%dT%H:%M:%SZ" (T.unpack string) :: Maybe T.UTCTime) of
          Nothing -> error "No due date set" -- Halt program if there's no due date
          Just parsedDate -> return (ediStudents, parsedDate)

  print ("Canvas Enrollments retrieved. There are " <> show (length enrollments) <> " active students in the Edinburgh section on the F27SB course")

  let usernames = extractUserNamesFromEnrollment enrollments
  let courseworkName = T.pack "coursework_1" -- Your coursework name on GitLab

  -- create issue title and description
  let issueTitle = T.pack "Deadline"
  let issueDescription = T.pack "The deadline for this assignment has been set to " <> (formatTime' dueDate) <> " but you will need to present your work to a lab helper during your assigned lab slot before this date and time to get the marks. It is your responsibility to approach a lab helper in person during your assigned lab session. After you have been marked, upload your code to GitLab."

  -- In the context of GitLab
  (gitlabUsers, gitlabProjects) <- runGitLab gitlabConfig $ do
    usrs <- getGitLabUsers usernames -- all students' GitLab user
    prjcts <- getGitLabUsersProjectsWithName usrs courseworkName -- list of users' projects
    mapM_
      ( \p -> do
          issues <- projectIssues p defaultIssueFilters
          case length issues of
            0 -> do
              void $ createNewIssue p issueTitle issueDescription dueDate
              liftIO (print (T.pack "New issue created for " <> owner_name (fromJust (project_owner p)))) -- print the name of the project
            _ -> liftIO $ print (T.pack "Issue exists for " <> owner_name (fromJust (project_owner p)))
      )
      prjcts

    return (usrs, prjcts)

  print (T.pack "Number of GitLab users: " <> T.pack (show (length gitlabUsers)))
  print (T.pack "Number of GitLab projects: " <> T.pack (show (length gitlabProjects)))

formatTime' :: T.UTCTime -> T.Text
formatTime' t = T.pack (T.formatTime T.defaultTimeLocale "%-d %B %Y at %R" t) -- 'rd' day suffix for now

extractUserNamesFromEnrollment :: [C.Enrollment] -> [T.Text]
extractUserNamesFromEnrollment [] = []
extractUserNamesFromEnrollment (enrollment : xs) = do
  let usr = fromJust (enrollmentUser enrollment)
  T.dropEnd 9 (safeNullNothing T.empty (userLoginId usr)) : extractUserNamesFromEnrollment xs

createNewIssue :: Project -> T.Text -> T.Text -> T.UTCTime -> GitLab (Maybe G.Issue)
createNewIssue gProject issueTitle issueDescription issueDeadline = do
  case project_creator_id gProject of
    Nothing -> return Nothing
    Just usrId -> do
      let attrs =
            (defaultIssueAttrs (project_id gProject))
              { set_issue_assignee_ids = Just [usrId],
                set_issue_due_date = Just issueDeadline,
                set_issue_labels = Just [T.pack "Deadline"]
              }
      result <- newIssue' (project_id gProject) issueTitle issueDescription attrs
      case result of
        Left _ -> do
          liftIO $ print (T.pack "Unsuccessful response when trying to create an issue.")
          return Nothing
        Right Nothing -> do
          liftIO $ print (T.pack "No issue returned when trying to create one.")
          return Nothing
        Right theIssue -> return theIssue

-- | Retrieve all projects of a user in a list of users with a certain project name
-- @params user The GitLab user whose projects to search.
-- @params projectName The string to look for in the project's title.
-- @returns A list of GitLab projects
getGitLabUsersProjectsWithName :: [G.User] -> T.Text -> GitLab [G.Project]
getGitLabUsersProjectsWithName gUsers projectName = do
  theProjects <- go gUsers
  return (catMaybes theProjects)
  where
    go :: [G.User] -> GitLab [Maybe G.Project]
    go [] = return []
    go (usr : xs) = do
      let attrs =
            defaultProjectSearchAttrs
              { projectSearchFilter_search = Just projectName -- look for projectName
              }
      result <- userProjects usr attrs
      case result of
        Nothing -> do
          others <- go xs
          return (Nothing : others)
        Just [] -> do
          -- user exists, but they haven't forked the project
          go xs
        Just usersProjects -> do
          others <- go xs
          return (Just (head usersProjects) : others) -- can safely use head because there will only be one project with that name

getGitLabUsers :: [T.Text] -> GitLab [G.User]
getGitLabUsers usernames = do
  go usernames
  where
    go :: [T.Text] -> GitLab [G.User]
    go [] = return []
    go (username : xs) = do
      result <- searchUser username
      case result of
        Nothing -> do
          liftIO (print (T.pack "No user with the username " <> username))
          go xs
        Just theUser -> do
          others <- go xs
          return (theUser : others)
