{-# LANGUAGE OverloadedStrings #-}

module Main
  ( main,
  )
where

import Canvas
import Common
import Control.Monad.IO.Class
import Data.List (nub, partition)
import Data.Maybe
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.Lazy as TL
import Data.Time.Clock
import Data.Time.Format.ISO8601
import GitLab
import Network.HTTP.Client
import Options.Applicative
import System.Environment
import System.ProgressBar (Progress (..), Style (..), defStyle, exact, incProgress, msg, newProgressBar)

data MultiCourseReportOpts = MultiCourseReportOpts
  { outFile :: Text,
    courseIdList :: Text
  }

parserOpts :: Parser MultiCourseReportOpts
parserOpts =
  MultiCourseReportOpts
    <$> strOption
      ( long "outFile"
          <> short 'o'
          <> metavar "FILENAME"
          <> help "the name of the CSV output file"
      )
    <*> strOption
      ( long "courseIds"
          <> short 'c'
          <> metavar "COURSE_IDS"
          <> help "A comma separated list of course IDs"
      )

main :: IO ()
main = do
  genReport =<< execParser opts
  where
    opts =
      info
        (parserOpts <**> helper)
        ( fullDesc
            <> progDesc "Generates a spreadsheet of Canvas and GitLab activity across multiple courses "
            <> header "multi-course-engagement -o FILENAME -c COURSE_IDs"
        )

genReport :: MultiCourseReportOpts -> IO ()
genReport opts = do
  gToken <- T.pack <$> getEnv "GITLAB_TOKEN"
  cToken <- T.pack <$> getEnv "CANVAS_TOKEN"
  let securityScheme = bearerAuthenticationSecurityScheme cToken
      canvasConfig =
        defaultConfiguration
          { configBaseURL = "https://canvas.hw.ac.uk/api",
            configSecurityScheme = securityScheme
          }
      gitlabConfig =
        defaultGitLabServer
          { url = "https://gitlab-student.macs.hw.ac.uk",
            token = gToken
          }
      courseIds = T.splitOn (T.pack ",") (courseIdList opts)

  pbCanvas <- newProgressBar (progressBarStyle "Canvas") 10 (System.ProgressBar.Progress 0 (length courseIds) ())

  allData <-
    runWithConfiguration canvasConfig $ do
      courseActivityData <-
        concat
          <$> mapM
            ( \theCourseId -> do
                liftIO (incProgress pbCanvas 1)
                lastCanvasCourseActivity theCourseId
            )
            courseIds

      let studentActivity =
            groupBy
              ( \(studentId1, _, _, _) (studentId2, _, _, _) ->
                  studentId1 == studentId2
              )
              courseActivityData

      pbGitLab <- liftIO (newProgressBar (progressBarStyle "GitLab") 10 (System.ProgressBar.Progress 0 (length studentActivity) ()))

      mapM
        ( \studentLoginData -> do
            let (studentId, studentName, _, _) = head studentLoginData
                courseActivityDates = map (\(_, _, theCourseId, datePerCourse) -> (theCourseId, datePerCourse)) studentLoginData
                username = T.takeWhile (/= '@') studentId -- drops "@hw.ac.uk"
            gitLabActivityDate <- liftIO (lastGitLabActivity gitlabConfig username)
            liftIO (incProgress pbGitLab 1)
            return (username, studentName, courseActivityDates, gitLabActivityDate)
        )
        studentActivity

  let orderedCourseIds = csvHeaders allData

  let fileContents = T.unpack (T.unlines [T.intercalate (T.pack ",") orderedCourseIds, T.unlines (dataToCsv (allCourseIds allData) allData)])

  writeFile (T.unpack (outFile opts)) fileContents

type Username = Text

type StudentName = Text

type CourseCode = Text

type ActivityDate = UTCTime

csvHeaders :: [(Username, StudentName, [(CourseCode, ActivityDate)], ActivityDate)] -> [Text]
csvHeaders xs =
  [ "username",
    "student name",
    "gitlab",
    T.intercalate (T.pack ",") (allCourseIds xs)
  ]

allCourseIds :: [(Username, StudentName, [(CourseCode, ActivityDate)], ActivityDate)] -> [Text]
allCourseIds xs =
  -- drops the last empty string to prevent trailing "," in the first row
  init (go xs)
  where
    go [] = [""]
    go ((_, _, courseData, _) : zs) =
      nub (map fst courseData <> go zs)

dataToCsv :: [Text] -> [(Username, StudentName, [(CourseCode, ActivityDate)], ActivityDate)] -> [Text]
dataToCsv _ [] = [""]
dataToCsv courseIds ((username, studentName, courseDates, gitlabDate) : xs) =
  T.intercalate
    (T.pack ",")
    [ username,
      studentName,
      T.pack (show (utctDay gitlabDate)),
      T.intercalate (T.pack ",") (extractDates courseIds)
    ]
    : dataToCsv courseIds xs
  where
    extractDates :: [Text] -> [Text]
    extractDates zs =
      -- drops the last empty string to prevent trailing "," in the
      -- student rows
      init (go zs)
      where
        go [] = [T.empty]
        go (theCourseId : ys) =
          case lookup theCourseId courseDates of
            Nothing -> ""
            Just theDate -> T.pack (show (utctDay theDate))
            : go ys

defaultDate :: UTCTime
defaultDate = read "1900-01-01 12:00:00.000000 UTC"

-- | gets the 'last_activity_at' value for the student's most recently
-- update GitLab Project. This function is quite slow, because
-- 'userProjects' gets all projects of a student, before picking the
-- first one, i.e. the most recent one. It'd be better to find a more
-- efficient GitLab API call to get only one project, which is the
-- most recently updated one.
lastGitLabActivity ::
  -- | GitLab server configuration
  GitLabServerConfig ->
  -- | GitLab username
  Text ->
  IO UTCTime
lastGitLabActivity gitlabConf gitLabUsername = do
  runGitLab gitlabConf $ do
    searchResult <- searchUser gitLabUsername
    case searchResult of
      Nothing -> return defaultDate
      Just student -> do
        projectSearch <- userProjects student (defaultProjectSearchAttrs {projectSearchFilter_order_by = Just UpdatedAt})
        case projectSearch of
          Nothing -> return defaultDate
          Just [] -> return defaultDate
          Just (prj : _) -> return (fromMaybe defaultDate (project_last_activity_at prj))

lastCanvasCourseActivity :: Text -> ClientM [(Username, StudentName, CourseCode, UTCTime)]
lastCanvasCourseActivity theCourseId = do
  -- first get the course code
  courseResult <- getSingleCourseCourses (mkGetSingleCourseCoursesParameters theCourseId)
  let courseCode =
        case responseBody courseResult of
          GetSingleCourseCoursesResponseError err ->
            error ("couldn't find course ID " <> show theCourseId <> " " <> err)
          GetSingleCourseCoursesResponse200 theCourse ->
            safeNullNothing "" (courseCourseCode theCourse)

  -- now get the student enrollment data
  let params =
        (mkListEnrollmentsCoursesParameters theCourseId)
          { listEnrollmentsCoursesParametersQueryState = Just ListEnrollmentsCoursesParametersQueryState'EnumActive,
            listEnrollmentsCoursesParametersQueryType = Just [T.pack "StudentEnrollment"]
          }

  enrollments <-
    do
      response <- listEnrollmentsCourses params
      case responseBody response of
        ListEnrollmentsCoursesResponseError err -> do
          -- let the program crash, as there must be something wrong in the parameters
          error ("Couldn't get enrollments for course: " <> show theCourseId <> ", reason: " <> err)
        ListEnrollmentsCoursesResponse200 theEnrollments -> return theEnrollments

  let activityData =
        map
          ( \enrollment ->
              case (enrollmentUser enrollment, enrollmentLastActivityAt enrollment) of
                (Just student, Just (NonNull activityDate)) ->
                  Just
                    ( safeNullNothing "" (userLoginId student),
                      safeNullNothing "" (userShortName student),
                      courseCode,
                      fromMaybe defaultDate (iso8601ParseM (T.unpack activityDate) :: Maybe UTCTime)
                    )
                _ -> Nothing
          )
          enrollments
  return (catMaybes activityData)

progressBarStyle :: Text -> Style ()
progressBarStyle name =
  defStyle {stylePrefix = msg (TL.fromStrict name), stylePostfix = exact}

-- groups lists of non-adjacent elements.
groupBy :: (a -> a -> Bool) -> [a] -> [[a]]
groupBy = go []
  where
    go acc _ [] = acc
    go acc comp (h : t) =
      let (hs, nohs) = partition (comp h) t
       in go ((h : hs) : acc) comp nohs
