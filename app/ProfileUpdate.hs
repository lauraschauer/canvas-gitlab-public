{-# LANGUAGE OverloadedStrings #-}

module Main (main) where

import Canvas
import Common
import Control.Monad
import Control.Monad.IO.Class
import Data.Maybe (catMaybes)
import qualified Data.Text as T
import GitLab
import Network.HTTP.Client
import System.Environment

data ProfileInfo = ProfileInfo
  { g_username :: T.Text, -- the GitLab username, extracted from the Canvas email
    c_bio :: T.Text, -- the Canvas Bio
    c_pronouns :: T.Text, -- the Canvas Pronouns
    c_sections :: T.Text, -- the Canvas Sections
    c_integrationId :: T.Text -- the Canvas H-Number
  }

main :: IO ()
main = do
  let cCourseId = "22222" -- Your Canvas Course ID

  -- Canvas Configuration
  cToken <- T.pack <$> getEnv "CANVAS_TOKEN"
  let securityScheme = bearerAuthenticationSecurityScheme cToken

  let canvasConfig =
        defaultConfiguration
          { configBaseURL = "https://canvas.hw.ac.uk/api",
            configSecurityScheme = securityScheme
          }

  -- In Canvas context
  profileInfos <- runWithConfiguration canvasConfig $ do
    -- Gets all users on a course (includes everyone, eg. lecturers, TAs and students)
    let params =
          (mkListUsersInCourseUsersParameters cCourseId)
            { listUsersInCourseUsersParametersQueryInclude__ = Just ListUsersInCourseUsersParametersQueryInclude__'EnumSections -- requests the user's enrollments, in addition to the regular profile fields
            }
    cUsers <-
      do
        response <- listUsersInCourseUsers params
        case responseBody response of
          ListUsersInCourseUsersResponseError err -> do
            -- let the program crash, as there must be something wrong in the parameters
            error ("Couldn't get users for course: " <> show cCourseId <> ", reason: " <> err)
          ListUsersInCourseUsersResponse200 theProfiles -> return theProfiles

    -- Get the username, loginId, bio, pronouns, sections, integrationId from their Canvas Profile
    profiles <-
      mapM
        ( \usr -> do
            let sections = safeNullNothing T.empty (userSections usr) -- all sections the user is part of
            do
              response <- getUserProfile (T.pack $ show (userId usr))
              case responseBody response of
                GetUserProfileResponseError err -> do
                  liftIO
                    ( print
                        ( "Could not get profile information from Canvas for "
                            <> T.unpack (safeNullNothing T.empty (userName usr))
                            <> ", reason: "
                            <> err
                        )
                    )
                  return Nothing
                GetUserProfileResponse200 theProfile -> return (Just (extractInformation sections theProfile))
        )
        cUsers

    -- return list of profiles without 'Nothing' values
    return (catMaybes profiles)

  print (T.pack "Retrieved " <> T.pack (show (length profileInfos)) <> T.pack " Canvas users.") -- sanity check

  -- GitLab Configuration
  gToken <- T.pack <$> getEnv "GITLAB_TOKEN"

  let gitlabConfig =
        defaultGitLabServer
          { url = "https://gitlab-student.macs.hw.ac.uk",
            token = gToken
          }

  -- In the context of GitLab
  void $ -- don't care about the result
    runGitLab gitlabConfig $ do
      go profileInfos
  where
    go :: [ProfileInfo] -> GitLab [GitLab.User]
    go [] = return []
    go (profile : xs) = do
      -- get the GitLab user
      result <- searchUser (g_username profile)
      case result of
        Nothing -> do
          -- print the user that has no GitLab account (might be useful to know)
          liftIO (print (T.pack "No GitLab user with the username " <> g_username profile))
          -- then ignore the issue and continue with the rest of the list
          go xs
        -- Good, found the user on GitLab
        Just gUser -> do
          -- create parameters to pass with the user modification request
          let params = createAttributes gUser profile

          -- modifies the user
          modifiedUser <- do
            response <- modifyUser (user_id gUser) params
            case response of
              Left _ -> do
                liftIO (print (T.pack "Could not modify " <> g_username profile)) -- message when modification didn't work
                return Nothing
              Right Nothing -> do
                liftIO (print (T.pack "Modified, but no user returned for " <> g_username profile))
                return Nothing
              Right (Just theUser) -> return (Just theUser)

          -- modifies the rest of the list
          case modifiedUser of
            -- if something went wrong, it has been printed out above, so ignore silently here
            Nothing -> do
              go xs
            -- user has been modified correctly, great, keep going :)
            Just theUser -> do
              others <- go xs
              return (theUser : others)

-- Parses Canvas Profile into a ProfileInfo object
extractInformation :: T.Text -> Canvas.Profile -> ProfileInfo
extractInformation cSections cProfile =
  let loginId = T.dropEnd 9 (safeNullNothing T.empty (profileLoginId cProfile))
      bio = safeNullNothing T.empty (profileBio cProfile)
      pronouns = safeNullNothing T.empty (profilePronouns cProfile)
      integrationId = safeNullNothing T.empty (profileIntegrationId cProfile)
   in ProfileInfo loginId bio pronouns cSections integrationId

-- creates attributes object which contains the parameters to send to a GitLab Modify User request
createAttributes :: GitLab.User -> ProfileInfo -> GitLab.UserAttrs
createAttributes gUser cProfileInfo =
  -- update with Canvas bio if there is none set on GitLab
  let bio =
        if T.null (maybe T.empty id (user_bio gUser))
          then c_bio cProfileInfo
          else maybe T.empty id (user_bio gUser)
      -- update with Canvas pronouns if none are set on GitLab
      pronouns =
        if T.null (maybe T.empty id (user_pronouns gUser))
          then c_pronouns cProfileInfo
          else maybe T.empty id (user_pronouns gUser)
      -- update with Canvas location if none is set on GitLab
      -- Remove if-statements temporarily to fix Dubai students' location
      location =
        if T.null (maybe T.empty id (user_location gUser))
          then setGitLabLocation (c_sections cProfileInfo)
          else maybe T.empty id (user_location gUser)
      -- add the H-Number to the organisation field on GitLab
      organisation =
        if T.null (maybe T.empty id (user_organization gUser))
          then c_integrationId cProfileInfo
          else maybe T.empty id (user_organization gUser)
   in defaultUserFilters
        { userFilter_bio = Just bio,
          userFilter_pronouns = Just pronouns,
          userFilter_location = Just location,
          userFilter_organization = Just organisation
        }

-- Returns the location
setGitLabLocation :: T.Text -> T.Text
setGitLabLocation cSections
  | T.isInfixOf (T.pack "Edinburgh") cSections = "Edinburgh"
  | T.isInfixOf (T.pack "Dubai") cSections = "Dubai"
  | T.isInfixOf (T.pack "Malaysia") cSections = "Malaysia"
  | T.isInfixOf (T.pack "OUC") cSections = "OUC"
  | otherwise = ""
