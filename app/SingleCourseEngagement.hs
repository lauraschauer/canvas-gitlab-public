{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
{-# OPTIONS_GHC -Wno-unused-do-bind #-}
{-# OPTIONS_GHC -Wno-unused-matches #-}

module Main
  ( main,
  )
where

import Canvas
import Common
import Control.Monad.IO.Class
import Data.List (partition)
import qualified Data.Map as Map
import Data.Maybe (catMaybes, fromJust, fromMaybe)
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.Lazy as TL
import Data.Time.Clock
import Data.Time.Format
import GitLab
import Network.HTTP.Client
import Options.Applicative
import System.Environment
import System.ProgressBar (Progress (..), Style (..), defStyle, exact, incProgress, msg, newProgressBar)

data SingleCourseReportOpts = SingleCourseReportOpts
  { outFile :: T.Text,
    cId :: T.Text,
    gGroupName :: T.Text
  }

-- Define command line tool options
parserOpts :: Parser SingleCourseReportOpts
parserOpts =
  SingleCourseReportOpts
    <$> strOption
      ( long "outFile"
          <> short 'o'
          <> metavar "FILENAME"
          <> help "The name of the CSV output file"
      )
    <*> strOption
      ( long "cCourseId"
          <> short 'c'
          <> metavar "COURSE_ID"
          <> help "The Canvas course ID for which the report should be generated"
      )
    <*> strOption
      ( long "gGroupName"
          <> short 'g'
          <> metavar "GROUP_NAME"
          <> help "The name of the group on GitLab corresponding to the Canvas course"
      )

main :: IO ()
main = do
  genReport =<< execParser opts
  where
    opts =
      info
        (parserOpts <**> helper)
        ( fullDesc
            <> progDesc "Generates a spreadsheet of Canvas and GitLab activity for assignments of one course "
            <> header "single-course-engagement -o FILENAME -c COURSE_ID -g GROUP_NAME"
        )

genReport :: SingleCourseReportOpts -> IO ()
genReport opts = do
  gToken <- T.pack <$> getEnv "GITLAB_TOKEN"
  cToken <- T.pack <$> getEnv "CANVAS_TOKEN"

  let securityScheme = bearerAuthenticationSecurityScheme cToken
      canvasConfig =
        defaultConfiguration
          { configBaseURL = "https://canvas.hw.ac.uk/api",
            configSecurityScheme = securityScheme
          }
      gitlabConfig =
        defaultGitLabServer
          { url = "https://gitlab-student.macs.hw.ac.uk",
            token = gToken
          }
      cCourseId = cId opts
      gitlabGroupName = gGroupName opts

  canvasData <- runWithConfiguration canvasConfig $ getCanvasData cCourseId

  gitlabData <- runGitLab gitlabConfig $ getGitlabData gitlabGroupName
  print gitlabData
  print (snd (head gitlabData))

  let csvHeaders = T.intercalate (T.pack ",") (genCSVHeaders (catMaybes canvasData) gitlabData)
  let fileContents = T.unpack (T.unlines [csvHeaders, T.unlines (dataToCSV (catMaybes canvasData) gitlabData)])

  writeFile (T.unpack (outFile opts)) fileContents

-- Generates the headers for the CSV file
genCSVHeaders :: [(Username, StudentName, Scores, ActivityDate)] -> [(Username, [ProjectData])] -> [Text]
genCSVHeaders [] _ = [T.empty]
genCSVHeaders _ [] = [T.empty]
genCSVHeaders (x : _) (y : _) =
  [ "Username",
    "Student Name",
    T.intercalate (T.pack ",") (allAssignmentIds x),
    "Last Activity Date",
    T.intercalate (T.pack ",") (allGitlabProjectMetrics (snd y))
  ]

-- Extracts the assignment names from [(T.Text, Double)] :: Scores
-- for example: [("CW1", 3.0), ("CW2", 4.5)] --> ["CW1", "CW2"]
allAssignmentIds :: (Username, StudentName, Scores, ActivityDate) -> [T.Text]
allAssignmentIds (_, _, scores, _) = map fst scores

-- Extracts the metric names from a ProjectData object
-- for example: [("cw-1", 10, "success", "private", "today")] --> ["cw-1 commits"]
allGitlabProjectMetrics :: [ProjectData] -> [T.Text]
allGitlabProjectMetrics [] = [T.pack "No GitLab data"]
allGitlabProjectMetrics theList =
  concatMap
    ( \(title, _, _, _, _) ->
        [ T.append title $ T.pack " commits",
          T.append title $ T.pack " pipeline",
          T.append title $ T.pack " visibility",
          T.append title $ T.pack " last activity"
        ]
    )
    theList

-- Turns the data into T.Text and a CSV format
dataToCSV :: [(Username, StudentName, Scores, ActivityDate)] -> [(Username, [ProjectData])] -> [T.Text]
dataToCSV [] _ = []
dataToCSV ((username, studentName, scores, activityDate) : xs) gitlabData =
  T.intercalate
    (T.pack ",")
    [ username,
      studentName,
      T.intercalate (T.pack ",") (extractScores scores),
      T.pack (show (utctDay activityDate)),
      T.intercalate (T.pack ",") (matchGitlabData username gitlabData)
    ] :
  dataToCSV xs gitlabData
  where
    -- Extracts the assignment scores from a list of Scores and turns them into a T.Text
    -- for example: [("CW1", 3.0), ("CW2", 4.5)] --> ["3.0", "4.5"]
    extractScores :: [(T.Text, Double)] -> [T.Text]
    extractScores [] = [T.empty]
    extractScores zs = map (T.pack . show . snd) zs
    -- Matches the corresponding GitLab data with the student's username
    matchGitlabData :: Username -> [(Username, [ProjectData])] -> [T.Text]
    matchGitlabData _ [] = [T.empty]
    matchGitlabData usrName theData = do
      let matchedData = filter (\stud -> usrName == fst stud) theData
      case matchedData of
        [] -> return $ T.intercalate (T.pack ",") [T.pack "No project found", T.empty, T.empty, T.empty]
        d -> return $ T.intercalate (T.pack ",") (turnToText $ snd (head d))

-- Turns a list or ProjectDatas into a list of Texts
-- eg. [("group-project-1",-1,"success","private",2023-09-10 12:09:41.314 UTC)] ->
-- [("group-project-1","-1","success","private","2023-09-10")]
turnToText :: [ProjectData] -> [T.Text]
turnToText [] = []
turnToText ((title, commits, pipe, visibility, last_activity) : xs) = [T.pack (show commits), pipe, visibility, T.pack $ show (utctDay last_activity)] ++ turnToText xs

-- | Obtains each student's score for each assignment on a Canvas course
getCanvasData ::
  -- | The Canvas course ID
  T.Text ->
  -- | Return a list of records containing the student's username, name, scores and last activity
  ClientM [Maybe (Username, StudentName, Scores, ActivityDate)]
getCanvasData cCourseId = do
  -- let the user know what's happening
  liftIO $ print (T.pack "Receiving Canvas Data (this may take a while)")
  -- get a list of all assignments on this course
  let assParams = mkListAssignmentsParameters cCourseId

  assResponse <- listAssignments assParams
  case responseBody assResponse of
    ListAssignmentsResponseError _ -> error ("Could not get assignments for the course with ID " <> T.unpack cCourseId)
    ListAssignmentsResponse200 theAssigns -> do
      -- get the students' submissions
      let params =
            (mkListSubmissionsForMultipleAssignmentsCoursesParameters cCourseId)
              { listSubmissionsForMultipleAssignmentsCoursesParametersQueryStudentIds__ = Just [T.pack "all"],
                listSubmissionsForMultipleAssignmentsCoursesParametersQueryInclude__ = Just ListSubmissionsForMultipleAssignmentsCoursesParametersQueryInclude__'EnumAssignment
              }

      response <- listSubmissionsForMultipleAssignmentsCourses params
      case responseBody response of
        ListSubmissionsForMultipleAssignmentsCoursesResponseError _ -> do
          error ("Couldn't get submissions for course :" <> show cCourseId)
        ListSubmissionsForMultipleAssignmentsCoursesResponse200 theSubs -> do
          -- group the records by user_id, so all of one student's submissions are in a group
          let groupedSubmissions =
                groupBy
                  ( \sub1 sub2 ->
                      safeNullNothing 0 (submissionUserId sub1) == safeNullNothing 1 (submissionUserId sub2)
                  )
                  theSubs

          pbProcessStudents <- liftIO (newProgressBar (progressBarStyle $ "Students on " <> cCourseId) 10 (System.ProgressBar.Progress 0 (length groupedSubmissions) ()))

          -- process each student and obtain their username, scores and last activity from the submissions
          mapM
            ( \student -> do
                liftIO $ incProgress pbProcessStudents 1
                processCanvasStudent student cCourseId theAssigns
            )
            groupedSubmissions

-- Creates a student record containing the username, name, score for each assignment, and
-- last-activity date from a batch of student submissions
processCanvasStudent :: [Submission] -> T.Text -> [Assignment] -> ClientM (Maybe (Username, StudentName, Scores, ActivityDate))
processCanvasStudent oneStudent cCourseId theAssigns = do
  -- obtains the map of assignments for the course
  -- Not every student has to submit every assignment: This map helps to fill the 'holes' in the spreadsheet caused by students' not having a submission for every assignments
  let gradedAssignments = filter (\a -> safeNullNothing T.empty (assignmentGradingType a) /= T.pack "not_graded") theAssigns
  let assignments = Map.fromList (map (\a -> (safeNullNothing (-1) (assignmentId a), (safeNullNothing T.empty (assignmentName a), Nothing))) gradedAssignments)

  -- obtains the actual scores for a student (this list might be shorter than `assignments`, caused by students not having a submission for every assignment)
  let scores =
        map
          ( \sub -> do
              let maybeAssignment = submissionAssignment sub
              case maybeAssignment of
                Nothing -> do
                  let assId = -2 -- to not risk matching with an actual assignment whose ID was not found
                  let assName = T.pack "No assignment"
                  (assId, assName, Nothing)
                Just theAss -> do
                  let assId = safeNullNothing (-3) (assignmentId theAss) -- to not risk matching with an actual assignment whose ID was not found
                  let assName = safeNullNothing (T.pack "Nameless assignment") (assignmentName theAss)
                  let score = safeNullNothing (-1) (submissionScore sub)
                  (assId, assName, Just score)
          )
          oneStudent

  let updatedScores =
        map
          ( \(_, (n, s)) -> do
              case s of
                Nothing -> (n, -1)
                Just sco -> (n, sco)
          )
          (Map.toList (updateMap assignments scores))

  -- obtain the student ID, to request this student's enrollment record for this course, which contains last_activity_at and name fields
  case submissionUserId (head oneStudent) of
    Nothing -> return Nothing
    Just Null -> return Nothing
    Just (NonNull theId) -> do
      -- request enrollment record for this student
      let params =
            (mkListEnrollmentsCoursesParameters cCourseId)
              { listEnrollmentsCoursesParametersQueryUserId = Just (T.pack $ show theId),
                listEnrollmentsCoursesParametersQueryRole = Just [T.pack "StudentEnrollment"] -- to ensure only student enrollments are returned
              }

      -- obtain the enrollments of the student
      response <- listEnrollmentsCourses params
      case responseBody response of
        ListEnrollmentsCoursesResponseError _ -> return Nothing -- error with API call
        ListEnrollmentsCoursesResponse200 [] -> return Nothing -- no enrollments for this student
        ListEnrollmentsCoursesResponse200 (oneEnrollment : _) -> do
          -- extract the last_activity_at info
          let activityString = safeNullNothing "1900-01-01T12:00:00Z" (enrollmentLastActivityAt oneEnrollment)
          let lastActivity = parseTimeToUTC (T.unpack activityString)

          -- extract the student from the enrollment response
          student <- do
            case enrollmentUser oneEnrollment of
              Just student -> return student
              Nothing -> error "No student user" -- TODO: silently ignore later

          -- get the student's name
          let studentName = safeNullNothing (T.pack "No username") (userName student)

          -- get the student's username
          let username = T.dropEnd 9 (safeNullNothing (T.pack "No username") (userLoginId student))

          let oneStudentData = (username, studentName, updatedScores, fromJust lastActivity) -- can safely use fromJust as the lastactivity is extracted with safeNullNothing and set to the default date in case the conversion does not work.
          return $ Just oneStudentData

type Username = Text

type StudentName = Text

type ActivityDate = UTCTime

type Scores = [(T.Text, Double)]

getGitlabData :: T.Text -> GitLab [(Username, [ProjectData])]
getGitlabData gGName = do
  -- get all projects and members of a group with the GitLab group name
  (allGroupProjects, allGroupMembers) <- do
    response <- searchGroup gGName
    case response of
      [] -> error "No group found"
      (theGroup : xs) -> do
        -- now get all of the group's projects
        let attrs =
              GroupProjectAttrs
                { groupProjectFilter_id = Nothing,
                  groupProjectFilter_archived = Nothing,
                  groupProjectFilter_visibility = Nothing,
                  groupProjectFilter_order_by = Nothing,
                  groupProjectFilter_sort = Nothing,
                  groupProjectFilter_search = Nothing,
                  groupProjectFilter_simple = Nothing,
                  groupProjectFilter_owned = Nothing,
                  groupProjectFilter_starred = Nothing,
                  groupProjectFilter_with_issues_enabled = Nothing,
                  groupProjectFilter_with_merge_requests_enabled = Nothing,
                  groupProjectFilter_with_shared = Nothing,
                  groupProjectFilter_include_subgroups = Nothing,
                  groupProjectFilter_min_access_level = Nothing,
                  groupProjectFilter_with_custom_attributes = Nothing,
                  groupProjectFilter_with_security_reports = Nothing
                }
        allProjects <- groupProjects theGroup attrs
        -- get all the group's members
        allMembers <- do
          eitherResponse <- membersOfGroup theGroup
          case eitherResponse of
            Left _ -> error "Could not get members"
            Right theMembers -> return theMembers

        return (allProjects, allMembers)

  -- get a list of names of all projects
  let projectNames = map project_name allGroupProjects

  -- goes through the list of members
  -- returns a [[Project]] with the Projects for each User
  mapM
    ( \mem -> do
        -- getting all projects of a user and filtering that list down to group projects
        theUser <- do
          response <- user (member_id mem) -- get the user
          case response of
            Left _ -> error "Could not get user"
            Right Nothing -> error "Could not get user"
            Right (Just myUser) -> return myUser
        -- get the user's projects
        let attrs = defaultProjectSearchAttrs
        theProjects <- do
          response <- userProjects theUser attrs
          case response of
            Nothing -> error "Could not get projects"
            Just allMyProjects -> return allMyProjects
        -- filter down the user's projects
        let userGroupProjects = filter (isInListOfProjectNames projectNames) theProjects
        liftIO $ print (user_username theUser)
        processGitLabStudent userGroupProjects (user_username theUser)
    )
    allGroupMembers

processGitLabStudent :: [Project] -> T.Text -> GitLab (Username, [ProjectData])
processGitLabStudent studentProjects studentUsername = do
  -- get the data we need
  prjData <-
    mapM
      ( \proj -> do
          -- get the project name
          let n = project_name proj
          -- get the commit count
          commits <- do
            case project_statistics proj of
              Nothing -> return (-1)
              Just theStats -> return (fromMaybe (-1) (statistics_commit_count theStats))
          -- get the project visibility
          let visibility = fromMaybe T.empty (project_visibility proj)
          -- get the last activity
          let last_activity = fromMaybe (fromJust (parseTimeToUTC "1900-01-01T12:00:00Z")) (project_last_activity_at proj)
          -- get the pipeline status
          pipelineStatus <- do
            result <- pipelines proj
            case result of
              [] -> return (T.pack "no pipelines available")
              (pip : _) -> return (pipeline_status pip)
          return (n, commits, pipelineStatus, visibility, last_activity)
      )
      studentProjects
  return (studentUsername, prjData)

-- ProjectData = [(project name, # commits, pipeline status, visibility, last_activity)]
type ProjectData = (T.Text, Int, T.Text, T.Text, ActivityDate)

-- turns a string containing a time into a UTCTime
parseTimeToUTC :: String -> Maybe UTCTime
parseTimeToUTC = parseTimeM True defaultTimeLocale "%Y-%m-%dT%H:%M:%SZ"

progressBarStyle :: Text -> Style ()
progressBarStyle name =
  defStyle {stylePrefix = msg (TL.fromStrict name), stylePostfix = exact}

-- groups a list of non-adjacent elements
groupBy :: (a -> a -> Bool) -> [a] -> [[a]]
groupBy = go []
  where
    go acc _ [] = acc
    go acc comp (h : t) =
      let (hs, nohs) = partition (comp h) t
       in go ((h : hs) : acc) comp nohs

-- fills the 'holes' in a student's submission list with dummy values
updateMap :: Map.Map Int (T.Text, Maybe Double) -> [(Int, T.Text, Maybe Double)] -> Map.Map Int (T.Text, Maybe Double)
updateMap oldMap [] = oldMap
updateMap oldMap ((_, _, Nothing) : xs) = updateMap oldMap xs
updateMap oldMap ((k, n, Just s) : xs) =
  case Map.lookup k oldMap of
    Nothing -> updateMap oldMap xs
    Just _ -> updateMap (Map.insert k (n, Just s) oldMap) xs

-- Predicate to check if project name is in the list of project names
-- TODO: add code to fill the 'holes' in case a student hasn't forked the project yet
isInListOfProjectNames :: [T.Text] -> Project -> Bool
isInListOfProjectNames names prjt = project_name prjt `elem` names
