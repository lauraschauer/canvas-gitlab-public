-- comment on a submission on Canvas
{-# LANGUAGE OverloadedStrings #-}

module Main
  ( main,
  )
where

import Canvas
import Common
import Control.Monad (void)
import Control.Monad.IO.Class (liftIO)
import qualified Data.Text as T
import GitLab
import Network.HTTP.Client
import System.Environment

main :: IO ()
main = do
  let cCourseId = "22222" -- Your Canvas Course ID

  -- Canvas Configuration
  cToken <- T.pack <$> getEnv "CANVAS_TOKEN"
  let securityScheme = bearerAuthenticationSecurityScheme cToken

  let canvasConfig =
        defaultConfiguration
          { configBaseURL = "https://canvas.hw.ac.uk/api",
            configSecurityScheme = securityScheme
          }

  -- GitLab Configuration
  gToken <- T.pack <$> getEnv "GITLAB_TOKEN"

  let gitlabConfig =
        defaultGitLabServer
          { url = "https://gitlab-student.macs.hw.ac.uk",
            token = gToken
          }

  -- In GitLab Context
  -- From Push trigger event, get the user_email -> extract "lms9"
  --                          get the project url to later add to the comment
  --                          get the project name to match with Canvas Assignment
  -- GitLab IDs (from Push trigger JSON): for now only given for proof of concept
  let gUserName = "lms9" -- my username, because 'test student' doesn't show up as a student
  let gProjectName = "Test_Laura" -- the name of the project from GitLab, to match with an assignment on Canvas
  let gProjectURL = "https://gitlab.com/lauraschauer/canvas-gitlab" -- the URL of the project from GitLab

  -- In Canvas Context
  (submission, subUser) <- runWithConfiguration canvasConfig $ do
    -- get user from Canvas with username
    let userParams =
          (mkListUsersInCourseUsersParameters cCourseId)
            { listUsersInCourseUsersParametersQuerySearchTerm = Just gUserName
            }

    studentUser <- do
      response <- listUsersInCourseUsers userParams
      case responseBody response of
        ListUsersInCourseUsersResponseError err -> error (show err)
        ListUsersInCourseUsersResponse200 theUsers -> return (head theUsers) -- get the first of the list as there is only one user associated with each username
    liftIO (print (safeNullNothing T.empty (userName studentUser))) -- sanity check

    -- get assignment for that user and project
    let testStudentId = "1111" -- for proof of concept
    -- let userId = (userId user)
    let assignmentParams =
          (mkListAssignmentsForUserParameters cCourseId testStudentId)
            { listAssignmentsForUserParametersQuerySearchTerm = Just gProjectName
            }
    assignment <- do
      response <- listAssignmentsForUser assignmentParams
      case responseBody response of
        ListAssignmentsForUserResponseError err -> error (show err)
        ListAssignmentsForUserResponse200 theAssignments -> return (head theAssignments)

    -- create the comment on the submission
    -- TODO: get submission object first and then change it
    -- TODO: add URL to description: "Commits were pushed, link here: <URL>"
    let submissionParams = mkGetSingleSubmissionCoursesParameters (T.pack (show (safeNullNothing (-1) (assignmentId assignment)))) cCourseId testStudentId

    studentSubmission <- do
      response <- getSingleSubmissionCourses submissionParams
      liftIO (print response)
      case responseBody response of
        GetSingleSubmissionCoursesResponseError err -> error (show err)
        GetSingleSubmissionCoursesResponse200 theSubmission -> return theSubmission

    -- parameters: assignmentId, courseId, userId
    let params = mkGradeOrCommentOnSubmissionCoursesParameters (T.pack (show (safeNullNothing (-1) (assignmentId assignment)))) cCourseId testStudentId

    -- request body
    let reqBody =
          mkGradeOrCommentOnSubmissionCoursesRequestBody
            { gradeOrCommentOnSubmissionCoursesRequestBodyCommentTextComment_ = Just "Hello, here's the URL",
              gradeOrCommentOnSubmissionCoursesRequestBodySubmissionPostedGrade_ = Just (safeNullNothing T.empty (submissionGrade studentSubmission)) -- keep the grade if it exists, otherwise don't send anything.
            }

    void $ do
      response <- gradeOrCommentOnSubmissionCourses params (Just reqBody)
      case responseBody response of
        GradeOrCommentOnSubmissionCoursesResponseError err -> error (show err)
        GradeOrCommentOnSubmissionCoursesResponse200 theSubmission -> return theSubmission

    return (studentSubmission, studentUser)

  print (safeNullNothing 0 (submissionScore submission))
  print (userId subUser)
