{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

module CommitAssignmentComment (ruleCommitAssignmentComment) where

import Canvas
import Common
import Control.Concurrent
import Control.Monad.IO.Class (liftIO)
import Data.Maybe
import qualified Data.Text as T
import GitLab
import Language.Haskell.TH.Env
import Network.HTTP.Client

-- | A server side rule that adds a comment to a Canvas assignment
-- corresponding a GitLab project. The comment indicates how many
-- commits were pushed, and by whom.
--
-- The code below has been written for the F20DP 2022/23 coursework.
ruleCommitAssignmentComment :: Rule
ruleCommitAssignmentComment =
  matchIf
    "Creates a comment on a Canvas assignment when a comit(s) for the associated GitLab project is pushed."
    ( \event@Push {} ->
        return $ projectEvent_name (push_project event) == T.pack "f20dp-cwk1"
    )
    ( \event@Push {} -> do
        -- GitLab Configuration
        let gTokenSearch = $$(envQ "GITLAB_TOKEN") :: Maybe String
        let gToken = fromMaybe "?" gTokenSearch
        let gitlabConfig =
              defaultGitLabServer
                { url = "https://gitlab-student.macs.hw.ac.uk",
                  token = T.pack gToken
                }

        let gProjectId = push_project_id event

        pipelineStatus <- liftIO $
          runGitLab gitlabConfig $ do
            -- get the project
            projectResult <- project gProjectId
            case projectResult of
              Left _ -> return (T.pack "not available") -- couldn't find project, give up
              Right Nothing -> return (T.pack "not available") -- something went wrong, give up
              Right (Just theProject) -> do
                -- give the server 3 seconds to realise a pipeline should be created
                liftIO (threadDelay 3000000)
                -- get the latest pipeline
                pipelineResult <- pipelines theProject
                case pipelineResult of
                  [] -> return (T.pack "not available") -- no pipelines exist
                  p : _ -> do
                    processPipeline p 0
                where
                  processPipeline _ 10 = return (T.pack "unknown")
                  processPipeline p n =
                    case pipeline_status p of
                      "success" -> return (T.pack "success")
                      "failed" -> return (T.pack "failed")
                      _ -> do
                        -- give the server 3 more seconds to finish the tests
                        liftIO (threadDelay 3000000)
                        -- try obtaining the result again
                        pipelineResult' <- pipeline theProject (pipeline_id p)
                        case pipelineResult' of
                          Left _ -> return (T.pack "not available")
                          Right Nothing -> return (T.pack "not available")
                          Right (Just p') -> do
                            processPipeline p' (n + 1)

        -- Canvas Configuration
        --
        -- Unlike `getEnv`, which uses the environment at *runtime*,
        -- this `envQ` function splices in the value at *compile
        -- time*. We use this because we compile this rule based
        -- program on a different machine before deploying to the
        -- gitlab-student server.
        let cTokenSearch = $$(envQ "CANVAS_TOKEN") :: Maybe String
        let cToken = fromMaybe "?" cTokenSearch
        let securityScheme = bearerAuthenticationSecurityScheme (T.pack cToken)
        let canvasConfig =
              defaultConfiguration
                { configBaseURL = T.pack "https://canvas.hw.ac.uk/api",
                  configSecurityScheme = securityScheme
                }

        -- username of the student who pushed commit(s)
        let gUserName = push_user_name event

        -- F20DP course ID
        let cCourseId = T.pack "22222" -- Your Canvas Course ID

        -- now move over to the Canvas stage
        liftIO $
          runWithConfiguration canvasConfig $ do
            -- get user from with username
            let userParams =
                  (mkListUsersInCourseUsersParameters cCourseId)
                    { listUsersInCourseUsersParametersQuerySearchTerm = Just gUserName
                    }

            maybeStudent <- do
              response <- listUsersInCourseUsers userParams
              case responseBody response of
                ListUsersInCourseUsersResponseError _err ->
                  return Nothing
                ListUsersInCourseUsersResponse200 [] ->
                  return Nothing
                ListUsersInCourseUsersResponse200 (theStudent : _) ->
                  -- get the first of the list as there should only be one user associated with each username
                  return (Just theStudent)

            case maybeStudent of
              Nothing ->
                -- GitLab student not found on Canvas, give up.
                return ()
              Just student -> do
                -- get assignment for that user and project
                let canvasAssignmentName = T.pack "coursework"

                let studentId = userId student
                let assignmentParams =
                      (mkListAssignmentsForUserParameters cCourseId (T.pack (show studentId)))
                        { listAssignmentsForUserParametersQuerySearchTerm = Just canvasAssignmentName
                        }
                maybeAssignment <- do
                  response <- listAssignmentsForUser assignmentParams
                  case responseBody response of
                    ListAssignmentsForUserResponseError _err ->
                      return Nothing
                    ListAssignmentsForUserResponse200 [] ->
                      return Nothing
                    ListAssignmentsForUserResponse200 (theAssignment : _) ->
                      return (Just theAssignment)
                case maybeAssignment of
                  Nothing ->
                    -- Assignment not found for this student, give up silently.
                    return ()
                  Just assignment -> do
                    -- create the comment on the submission
                    let submissionParams =
                          mkGetSingleSubmissionCoursesParameters
                            (T.pack (show (safeNullNothing (-1) (assignmentId assignment))))
                            cCourseId
                            (T.pack (show studentId))

                    existingSubmission <- do
                      response <- getSingleSubmissionCourses submissionParams
                      case responseBody response of
                        GetSingleSubmissionCoursesResponseError _err ->
                          return Nothing
                        GetSingleSubmissionCoursesResponse200 theSubmission ->
                          return (Just theSubmission)

                    -- parameters: assignmentId, courseId, userId
                    let params =
                          mkGradeOrCommentOnSubmissionCoursesParameters
                            (T.pack (show (safeNullNothing (-1) (assignmentId assignment))))
                            cCourseId
                            (T.pack (show studentId))

                    -- request body
                    let reqBody =
                          mkGradeOrCommentOnSubmissionCoursesRequestBody
                            { gradeOrCommentOnSubmissionCoursesRequestBodyCommentTextComment_ =
                                Just
                                  ( push_user_name event
                                      <> T.pack " pushed "
                                      <> T.pack (show (push_total_commits_count event))
                                      <> T.pack " "
                                      <> ( if push_total_commits_count event > 1
                                             then "commits"
                                             else "commit"
                                         )
                                      <> " to "
                                      <> projectEvent_web_url (push_project event)
                                      <> ". The latest pipeline's status is "
                                      <> pipelineStatus
                                      <> "."
                                  ),
                              gradeOrCommentOnSubmissionCoursesRequestBodySubmissionPostedGrade_ =
                                -- keep the grade if it exists,
                                -- otherwise don't send anything.
                                Just $ case existingSubmission of
                                  Nothing -> T.empty
                                  Just theSubmission ->
                                    safeNullNothing T.empty (submissionGrade theSubmission)
                            }

                    response <- do
                      gradeOrCommentOnSubmissionCourses
                        params
                        (Just reqBody)
                    case responseBody response of
                      GradeOrCommentOnSubmissionCoursesResponseError _err ->
                        -- HTTP error, oh well.
                        return ()
                      GradeOrCommentOnSubmissionCoursesResponse200 _theSubmission ->
                        -- all went well, great.
                        return ()
    )
