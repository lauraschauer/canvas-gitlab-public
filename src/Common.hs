{-# LANGUAGE OverloadedStrings #-}

module Common
  ( extractNonNull,
    extractNonNullList,
    safeNullNothing,
  )
where

import Canvas
import qualified Data.Text as T

extractNonNull :: Nullable T.Text -> T.Text
extractNonNull nullableStr = case nullableStr of
  NonNull str -> str
  Null -> ""

extractNonNullList :: Nullable [T.Text] -> [T.Text]
extractNonNullList nullableList = case nullableList of
  NonNull list -> list
  Null -> []

-- Function to safely extract a value from Maybe (Nullable a)
safeNullNothing :: a -> Maybe (Nullable a) -> a
safeNullNothing x Nothing = x
safeNullNothing x (Just Null) = x
safeNullNothing _ (Just (NonNull a)) = a
