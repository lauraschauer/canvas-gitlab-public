{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

module UpdateProfile (ruleUpdateProfile) where

import Canvas
import Common
import Control.Monad.IO.Class
import Data.Maybe
import qualified Data.Text as T
import GitLab
import Language.Haskell.TH.Env
import Network.HTTP.Client

data ProfileInfo = ProfileInfo
  { g_username :: T.Text, -- the GitLab username, extracted from the Canvas email
    c_bio :: T.Text, -- the Canvas Bio
    c_pronouns :: T.Text, -- the Canvas Pronouns
    c_sections :: T.Text, -- the Canvas Sections
    c_integrationId :: T.Text -- the Canvas H-Number
  }

-- | A server side rule that updates the newly created Gitlab profile with
-- information from the student's Canvas account.
ruleUpdateProfile :: Rule
ruleUpdateProfile =
  match -- TODO: not sure if 'match' instead of 'matchIf' is correct
    "Updates a student's profile when a new account is created."
    ( \event@UserCreate {} -> do
        -- Canvas Configuration
        --
        -- Unlike `getEnv`, which uses the environment at *runtime*,
        -- this `envQ` function splices in the value at *compile
        -- time*. We use this because we compile this rule based
        -- program on a different machine before deploying to the
        -- gitlab-student server.
        let cTokenSearch = $$(envQ "CANVAS_TOKEN") :: Maybe String
        let cToken = fromMaybe "?" cTokenSearch
        let securityScheme = bearerAuthenticationSecurityScheme (T.pack cToken)
        let canvasConfig =
              defaultConfiguration
                { configBaseURL = T.pack "https://canvas.hw.ac.uk/api",
                  configSecurityScheme = securityScheme
                }

        -- username of the student whose account was created
        let username = userCreate_username event

        -- To search for a student record through the API with only their username, a Canvas account-level token is needed. To work around this problem, this program currently obtains a student record through the course API. This means that students outside of this course will not be found.
        let cCourseId = "1111" -- practise course

        -- move to the Canvas stage
        profile <-
          liftIO $
            runWithConfiguration canvasConfig $ do
              -- searches for the user object, which includes their Canvas user ID (needed for the 'Get user profile' endpoint which includes the bio, pronouns, ...)
              let profileParams =
                    (mkListUsersInCourseUsersParameters cCourseId)
                      { listUsersInCourseUsersParametersQueryInclude__ = Just ListUsersInCourseUsersParametersQueryInclude__'EnumSections, -- requests the user's enrollments, in addition to the regular profile fields
                        listUsersInCourseUsersParametersQuerySearchTerm = Just username
                      }

              cUser <- do
                response <- listUsersInCourseUsers profileParams
                case responseBody response of
                  ListUsersInCourseUsersResponseError err -> do
                    -- TODO: not sure if the program can just crash
                    error ("Couldn't get users for course: " <> show cCourseId <> ", reason: " <> err)
                  ListUsersInCourseUsersResponse200 theProfiles -> return (head theProfiles) -- should only be one, since the request is searching for a user with the exact username -> TODO: not sure if there's a safer option for 'head'

              -- now in possession of the user ID, we can obtain the username, loginId, bio, pronouns, sections, integrationId from the student's Canvas profile
              let sections = safeNullNothing T.empty (userSections cUser)

              response <- getUserProfile (T.pack $ show (userId cUser))
              case responseBody response of
                GetUserProfileResponseError err -> do
                  -- TODO: again, not sure if the program can just crash
                  error
                    ( "Could not get profile information from Canvas for "
                        <> T.unpack (safeNullNothing T.empty (userName cUser))
                        <> ", reason: "
                        <> err
                    )
                GetUserProfileResponse200 theProfile -> return (extractInformation sections theProfile)

        -- move to the GitLab stage
        let gSearchToken = $$(envQ "GITLAB_TOKEN") :: Maybe String
        let gToken = fromMaybe "?" gSearchToken

        let gitlabConfig =
              defaultGitLabServer
                { url = "https://gitlab-student.macs.hw.ac.uk",
                  token = T.pack gToken
                }

        _ <- liftIO $
          runGitLab gitlabConfig $ do
            -- get the GitLab user
            result <- searchUser (g_username profile)
            case result of
              Nothing -> error "Couldn't find user" -- this should be impossible
              Just gUser -> do
                -- create parameters to pass with the user modification request
                let params = createAttributes gUser profile

                -- modifies the user
                response <- modifyUser (user_id gUser) params
                case response of
                  -- TODO: again, not sure if the program can just crash
                  Left _ -> error "Couldn't modify user"
                  Right Nothing -> error "Couldn't modify user"
                  Right (Just theUser) -> return (Just theUser)

        return ()
    )

-- Parses Canvas Profile into a ProfileInfo object
extractInformation :: T.Text -> Canvas.Profile -> ProfileInfo
extractInformation cSections cProfile =
  let loginId = T.dropEnd 9 (safeNullNothing T.empty (profileLoginId cProfile))
      bio = safeNullNothing T.empty (profileBio cProfile)
      pronouns = safeNullNothing T.empty (profilePronouns cProfile)
      integrationId = safeNullNothing T.empty (profileIntegrationId cProfile)
   in ProfileInfo loginId bio pronouns cSections integrationId

-- creates attributes object which contains the parameters to send to a GitLab Modify User request
createAttributes :: GitLab.User -> ProfileInfo -> GitLab.UserAttrs
createAttributes gUser cProfileInfo =
  -- update with Canvas bio if there is none set on GitLab
  let bio =
        if T.null (maybe T.empty id (user_bio gUser))
          then c_bio cProfileInfo
          else maybe T.empty id (user_bio gUser)
      -- update with Canvas pronouns if none are set on GitLab
      pronouns =
        if T.null (maybe T.empty id (user_pronouns gUser))
          then c_pronouns cProfileInfo
          else maybe T.empty id (user_pronouns gUser)
      -- update with Canvas location if none is set on GitLab
      -- Remove if-statements temporarily to fix Dubai students' location
      location =
        if T.null (maybe T.empty id (user_location gUser))
          then setGitLabLocation (c_sections cProfileInfo)
          else maybe T.empty id (user_location gUser)
      -- add the H-Number to the organisation field on GitLab
      organisation =
        if T.null (maybe T.empty id (user_organization gUser))
          then c_integrationId cProfileInfo
          else maybe T.empty id (user_organization gUser)
   in defaultUserFilters
        { userFilter_bio = Just bio,
          userFilter_pronouns = Just pronouns,
          userFilter_location = Just location,
          userFilter_organization = Just organisation
        }

-- Returns the location of the student
setGitLabLocation :: T.Text -> T.Text
setGitLabLocation cSections
  | T.isInfixOf (T.pack "Edinburgh") cSections = "Edinburgh"
  | T.isInfixOf (T.pack "Dubai") cSections = "Dubai"
  | T.isInfixOf (T.pack "Malaysia") cSections = "Malaysia"
  | T.isInfixOf (T.pack "OUC") cSections = "OUC"
  | otherwise = ""
