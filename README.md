# An integration between the Canvas and GitLab APIs

This repository contains programs to combine and exchange data between the Canvas and GitLab APIs.

The integration provides the following features:

- Imports Canvas assignment deadline into GitLab
- Creates spreadsheets containing Canvas and GitLab student interaction data
  - for multiple courses
  - or more detailed for a single course
- Connects Canvas assignments with according GitLab projects
- Synchronises User profiles across Canvas and GitLab

This integration uses the Haskell client libraries for [Canvas](https://gitlab.com/lauraschauer/canvas-haskell-library) and [GitLab](https://gitlab.com/robstewart57/gitlab-haskell).

## Example

If you want to create your own integration program, you can start with this code:

```haskell
import Canvas
import GitLab

main :: IO ()
main = do

  -- interaction with Canvas 
  let securityScheme = bearerAuthenticationSecurityScheme "your-token"
  let canvasConfig = 
      defaultConfiguration 
        { configBaseURL = "your-canvas-URL",
          configSecurityScheme = securitySCheme
        }

  runWithConfiguration canvasConfig $ do 
    -- your program here

  -- interaction with GitLab 
  let gitlabConfig = 
      defaultGitLabServer 
        { url = "your-gitlab-URL",
          tokenn = "your-gitlab-token"
        }
  
  runGitLab gitlabConfig $ do
    -- your program here
```

## Contributing

Contributions are welcome! Feel free to create an issue if you encounter any bugs or have feature requests!

## Integration use

This integration was initially developed to automate and support computer science
education. See our ICSE-SEET 2024 paper for the details: [_"Integrating Canvas
and GitLab to Enrich Learning
Processes"_](https://doi.org/10.1145/3639474.3640056).
